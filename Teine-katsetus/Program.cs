﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teine_katsetus
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Teine katsetus!");
            Console.WriteLine("Muudan Thea programmi.");
            // ei saanud saata, kuna oli vales repos? vaata järele, mis läks valesti

            //kolme operandiga tehte näide ?: ehk tingimustehe
            Console.WriteLine("Kuhu pean täna minema? Ole hea ja ütle:   " +(
                DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "Jooksma"
                : DateTime.Now.DayOfWeek == DayOfWeek.Tuesday ? "ujuma"
                : DateTime.Now.DayOfWeek == DayOfWeek.Tuesday ? "kinno"
                : "sauna"
                ));

            //ülesanded teise päeva slaidilt 16
            // küsib kas on võrdsed ==

            int a = 1;
            int b = 1;
            int c = 3;
            Console.WriteLine(a == b);
            Console.WriteLine(a == c);
            a = c;
            Console.WriteLine(a == b);
            Console.WriteLine(a == c);
            // tehted lähevad vastupidi, kuna a väärtus muutub

            int x1 = 10;
            int x2 = 20;
            int y1 = ++x1;
            int y2 = x2++;
            Console.WriteLine(x1);
            Console.WriteLine(x2);
            Console.WriteLine(y1);
            Console.WriteLine(y2);
            // ++ enne teeb tehte ja siis vaatab, mis saab
            // ++ pärast vaatab enne ja siis teeb tehte



        }
    }
}
